import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:mobx_slidy/app/app_widget.dart';
import 'package:mobx_slidy/app/modules/home/SegundaPagina.dart';
import 'package:mobx_slidy/app/modules/home/TerceiraPagina.dart';
import 'package:mobx_slidy/app/modules/home/home_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: HomeModule()),
        ModularRouter('/SegundaPagina', child: (_, args) => SegundaPagina()),
        ModularRouter('/TerceiraPagina', child: (_, args) => TerceiraPagina()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
