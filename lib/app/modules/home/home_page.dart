import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx_slidy/app/modules/home/SegundaPagina.dart';
import 'home_controller.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Navegando entre paginas"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Align(
            alignment: Alignment.center,
            child: Text("Teste De Navegacao por Rotas", style: TextStyle(fontSize: 22),),
          ),
          RaisedButton(
            color: Colors.purple,
              child: Text(
                  "Aperte o botao",
                style: TextStyle(color: Colors.white, fontSize: 22),
              ),
              onPressed: () {
                Modular.to.pushNamed("/SegundaPagina");
              }
              ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Modular.to.pushNamed("/TerceiraPagina");
        },
        child: Icon(Icons.add)
      ),
    );
  }
}
