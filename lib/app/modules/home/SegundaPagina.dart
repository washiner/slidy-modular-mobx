import 'package:flutter/material.dart';

class SegundaPagina extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("parabens vc esta na sua nova pagina"),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            transform: Matrix4.rotationZ(0.15),
            margin: EdgeInsets.all(30),
            color: Colors.purple,
            height: 200,
            width: 250,
          ),
        ],
      ),
    );
  }
}
