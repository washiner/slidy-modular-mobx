import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx_slidy/app/modules/home/home_controller.dart';

class TerceiraPagina extends StatefulWidget {
  @override
  _TerceiraPaginaState createState() => _TerceiraPaginaState();
}

class _TerceiraPaginaState extends State<TerceiraPagina> {
  final HomeController contador = HomeController();
  String titulo = "Washiner Takeuchi";
  String txt_curso = "SLIDY-MODULAR-MOBX";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter com MobX'),
      ),
      endDrawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.purple.shade600,
              ),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Image(
                  image: AssetImage("lib/assets/avatar.png"),
                ),
              ),
            ),
            SizedBox(height: 40),
            Center(
              child: RaisedButton(
                  color: Colors.black,
                  child: Text(
                    "Voltar para Home",
                    style: TextStyle(
                        color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                  onPressed: () {
                    Modular.to.pushNamed("/");
                  }),
            )
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              txt_curso,
              style: TextStyle(
                fontSize: 30.0,
              ),
            ),
            Observer(
              builder: (_) => Text(
                '${contador.value}',
                style: TextStyle(
                  fontSize: 42.0,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.add),
                  label: Text('Adicionar',style: TextStyle(fontSize: 30),),
                  onPressed: () {
                    contador.increment();
                  },
                ),
                FlatButton.icon(
                  icon: Icon(Icons.remove),
                  label: Text('Diminuir', style: TextStyle(fontSize: 30),),
                  onPressed: () {
                    contador.decrement();
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
